package com.example.moviebrowser.views.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.example.moviebrowser.R
import com.example.moviebrowser.callbacks.MovieDetailsCallback
import com.example.moviebrowser.callbacks.NowPlayingCallback
import com.example.moviebrowser.databinding.NowPlayingListFragmentBinding
import com.example.moviebrowser.models.Result
import com.example.moviebrowser.viewmodels.MainViewModel
import com.example.moviebrowser.views.adapters.MoviesListAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NowPlayingListFragment(private var callback : MovieDetailsCallback) : Fragment(), NowPlayingCallback {

    private lateinit var adapter : MoviesListAdapter
    private lateinit var binding : NowPlayingListFragmentBinding

    private val viewModel : MainViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_now_playing_list,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val response = viewModel.responseListLiveData.value

        setMoviesListAdapter(response ?: arrayListOf())
    }

    private fun setMoviesListAdapter(result : ArrayList<Result>) {
        adapter = MoviesListAdapter(result, this)
        binding.recyclerView.layoutManager = GridLayoutManager(activity,2)
        binding.recyclerView.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    private fun goToMovieDetails(item : Result) {
        //tutaj trzeba wywołać
        callback.setMovieDetails(item)
    }

    override fun onClickMovie(item: Result) {
        goToMovieDetails(item)
    }

}