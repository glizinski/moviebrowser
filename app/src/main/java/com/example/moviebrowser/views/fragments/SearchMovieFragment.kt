package com.example.moviebrowser.views.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.example.moviebrowser.R
import com.example.moviebrowser.callbacks.MovieDetailsCallback
import com.example.moviebrowser.callbacks.NowPlayingCallback
import com.example.moviebrowser.databinding.NowPlayingListFragmentBinding
import com.example.moviebrowser.databinding.SearchMovieFragmentBinding
import com.example.moviebrowser.models.Result
import com.example.moviebrowser.viewmodels.MainViewModel
import com.example.moviebrowser.views.adapters.MoviesListAdapter

class SearchMovieFragment(private var callback : MovieDetailsCallback) : Fragment(), NowPlayingCallback {

    private lateinit var adapter : MoviesListAdapter

    private lateinit var binding : SearchMovieFragmentBinding
    private val viewModel : MainViewModel by activityViewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search_movie,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setObservableMoviesAdapter()
        listeners()
        observable()
    }

    private fun listeners(){
        binding.buttonSearch.setOnClickListener{
            binding.editText.onEditorAction(EditorInfo.IME_ACTION_DONE)
            viewModel.getSearchingMoviesInViewModel(binding.editText.text.toString())
        }
    }

    private fun observable() {
        viewModel.responseSearchedListLiveData.observe(viewLifecycleOwner) {
            setObservableMoviesAdapter()
        }
    }

    private fun setObservableMoviesAdapter() {
        val response = viewModel.responseSearchedListLiveData.value
        setMoviesListAdapter(response ?: arrayListOf())
    }

    private fun setMoviesListAdapter(result : ArrayList<Result>) {
        adapter = MoviesListAdapter(result, this) //TODO
        binding.recyclerViewInSearch.layoutManager = GridLayoutManager(activity,2)
        binding.recyclerViewInSearch.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    private fun goToMovieDetails(item : Result) {
        //tutaj trzeba wywołać
        callback.setMovieDetails(item)
    }

    override fun onClickMovie(item: Result) {
        goToMovieDetails(item)
    }

    companion object {

    }
}