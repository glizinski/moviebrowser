package com.example.moviebrowser.views.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.viewpager2.widget.ViewPager2
import com.example.moviebrowser.R
import com.example.moviebrowser.callbacks.MovieDetailsCallback
import com.example.moviebrowser.databinding.MainViewPagerFragmentBinding
import com.example.moviebrowser.databinding.SplashFragmentBinding
import com.example.moviebrowser.models.Result
import com.example.moviebrowser.views.adapters.PagerAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class MainViewPagerFragment : Fragment(), MovieDetailsCallback {

    private lateinit var binding: MainViewPagerFragmentBinding

    lateinit var pagerAdapter : PagerAdapter
    lateinit var tabLayout : TabLayout
    lateinit var viewPager: ViewPager2

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_main_view_pager, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tabLayout = binding.tabLayout
        viewPager = binding.viewPager

        pagerAdapter = PagerAdapter(childFragmentManager, 3, lifecycle, this)

        viewPager.adapter = pagerAdapter
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            tab.text = "${position + 1}"
        }.attach()
    }

    private fun goToMovieDetails(item : Result) {
        val newFragment: Fragment = MovieDetailsFragment.newInstance(item)
        parentFragmentManager.beginTransaction().replace(R.id.frameLayout, newFragment)
            .addToBackStack("details")
            .commit()
    }


    companion object {
        @JvmStatic
        fun newInstance() =
            MainViewPagerFragment().apply {
            }
    }

    override fun setMovieDetails(item: Result) {
        goToMovieDetails(item)
    }
}