package com.example.moviebrowser.views.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.moviebrowser.callbacks.MovieDetailsCallback
import com.example.moviebrowser.views.fragments.NowPlayingListFragment
import com.example.moviebrowser.views.fragments.SearchMovieFragment

class PagerAdapter(
    fragmentManager : FragmentManager,
    private val numberOfFragments : Int,
    lifecycle: Lifecycle,
    private var callback : MovieDetailsCallback
) : FragmentStateAdapter(fragmentManager, lifecycle) {

    override fun getItemCount(): Int {
        return numberOfFragments
    }

    override fun createFragment(position: Int): Fragment {
        when (position) {
            0 -> return NowPlayingListFragment(callback)
            1 -> return SearchMovieFragment(callback)
            2 -> return NowPlayingListFragment(callback)
        }
        throw IllegalStateException("Position $position is illegal")
    }
}