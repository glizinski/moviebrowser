package com.example.moviebrowser.views.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.example.moviebrowser.BuildConfig
import com.example.moviebrowser.R
import com.example.moviebrowser.databinding.MovieDetailsFragmentBinding
import com.example.moviebrowser.databinding.NowPlayingListFragmentBinding
import com.example.moviebrowser.models.Result

class MovieDetailsFragment : Fragment() {

    private lateinit var item : Result
    private lateinit var binding : MovieDetailsFragmentBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setMovieDetails(item)

    }

    private fun setMovieDetails(item : Result) {
        binding.movieNameDetails.text = item.title ?: ""
        binding.releaseDateDetails.text = item.releaseDate
        binding.languageTextDetails.text = item.originalLanguage
        binding.ratingTextDetails.text = item.voteAverage.toString()
        binding.rateCountTextDetails.text = item.voteCount.toString()
        binding.MovieDetails.text = item.overview
        Glide
            .with(binding.root.context)
            .load(BuildConfig.IMAGE_LINK + item.posterPath)
            .fitCenter()
            .error(R.drawable.image_not_found)
            .into(binding.logoImageViewDetails)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_movie_details,container,false)
        return binding.root
    }

    companion object {
        @JvmStatic
        fun newInstance(movieDetails : Result) =
            MovieDetailsFragment().apply {
                item = movieDetails
            }
    }
}