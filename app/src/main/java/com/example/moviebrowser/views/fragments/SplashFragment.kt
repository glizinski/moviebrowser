package com.example.moviebrowser.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.moviebrowser.R
import com.example.moviebrowser.databinding.SplashFragmentBinding
import com.example.moviebrowser.viewmodels.MainViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashFragment : Fragment() {
    private lateinit var binding : SplashFragmentBinding
    private val viewModel: MainViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_splash,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getNowPlayingInViewModel()
        observable()
    }



    /**
    metoda obserwuje zmiane pola "responseListLiveData"
     */
    private fun observable() {
        viewModel.responseListLiveData.observe(viewLifecycleOwner) {
            goToNowPlayingListFragment()
        }
    }

    private fun goToNowPlayingListFragment() {
        val newFragment: Fragment = MainViewPagerFragment.newInstance()
        parentFragmentManager.beginTransaction().replace(R.id.frameLayout, newFragment).commit()
    }

    companion object {
        /*
        metoda statyczna newInstance
         */
        @JvmStatic
        fun newInstance() =
            SplashFragment().apply {
            }
    }
}


