package com.example.moviebrowser.views.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.moviebrowser.BuildConfig
import com.example.moviebrowser.R
import com.example.moviebrowser.callbacks.FavouriteStateMovieCallback
import com.example.moviebrowser.callbacks.NowPlayingCallback
import com.example.moviebrowser.databinding.MovieRowBinding
import com.example.moviebrowser.models.Result
import com.mikhaellopez.rxanimation.RxAnimation
import com.mikhaellopez.rxanimation.press
import kotlin.collections.ArrayList

class MoviesListAdapter(
    var arrayList: ArrayList<Result>,
    private var callback: NowPlayingCallback,
    private var stateCallback: FavouriteStateMovieCallback
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val adapterRow = DataBindingUtil.inflate<ViewDataBinding>(
            LayoutInflater.from(parent.context),
            R.layout.movie_row,
            parent,
            false
        )
        return MovieView(adapterRow as MovieRowBinding, callback, stateCallback)
    }

    class MovieView(
        private var binding: MovieRowBinding,
        private var callback: NowPlayingCallback,
        private var stateCallback: FavouriteStateMovieCallback
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Result) {

            checkStateFavoriteButton(item)

            binding.movieName.text = item.title ?: ""
            binding.releaseDate.text = item.releaseDate
            binding.languageText.text = item.originalLanguage
            binding.ratingText.text = item.voteAverage.toString()
            binding.rateCountText.text = item.voteCount.toString()

            Glide
                .with(binding.root.context)
                .load(BuildConfig.IMAGE_LINK + item.posterPath)
                .placeholder(R.drawable.image_not_found)
                .fitCenter()
                .error(R.drawable.image_not_found)
                .into(binding.logoImageView)

            binding.favoriteIcon.setOnClickListener {
                if (item.state) {
                    binding.favoriteIcon.setImageResource(R.drawable.ic_favorite_star_white)
                    item.state = false
                } else {
                    binding.favoriteIcon.setImageResource(R.drawable.ic_favorite_star_act)
                    item.state = true
                }
                stateCallback.setStateFavoriteMovie()
            }

            binding.cardView.setOnClickListener {
                RxAnimation.from(it).press(duration = 300).subscribe {
                    callback.onClickMovie(item)
                }
            }

            /*al watchersCountText = binding.root.context.getString(R.string.watchers_count)
            binding.watchers.text = "$watchersCountText ${item.watchersCount}"

            binding.cardView.setOnClickListener {
                RxAnimation.from(it).press(duration = 300).subscribe {
                    callback.onClickRepo(item)
                }
            }*/
        }

        private fun checkStateFavoriteButton(item : Result){
            if (!item.state) {
                binding.favoriteIcon.setImageResource(R.drawable.ic_favorite_star_white)
            } else {
                binding.favoriteIcon.setImageResource(R.drawable.ic_favorite_star_act)
            }
        }
    }


    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as MovieView
        val result = arrayList[position]
        holder.bind(result)
    }

    override fun getItemCount(): Int {
        return if (arrayList.isNullOrEmpty()) 0 else arrayList.size
    }
}