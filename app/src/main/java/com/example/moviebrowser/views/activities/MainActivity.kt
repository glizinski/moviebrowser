package com.example.moviebrowser.views.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.example.moviebrowser.views.adapters.PagerAdapter
import androidx.viewpager2.widget.ViewPager2
import com.example.moviebrowser.R
import com.example.moviebrowser.databinding.MainActivityBinding
import com.example.moviebrowser.views.fragments.SplashFragment
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startMainFragment()
        supportActionBar?.hide()



    }

    private fun startMainFragment() {
        val newFragment: Fragment = SplashFragment.newInstance()
        val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frameLayout, newFragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}