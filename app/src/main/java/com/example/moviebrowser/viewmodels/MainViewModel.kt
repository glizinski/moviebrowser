package com.example.moviebrowser.viewmodels

import androidx.lifecycle.ViewModel
import com.example.moviebrowser.models.NowPlayingResponseItem
import com.example.moviebrowser.models.Result
import com.example.moviebrowser.repositories.GetNowPlayingRepository
import com.example.moviebrowser.repositories.SearchedMoviesRepository
import com.example.moviebrowser.utils.SingleLiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    var getNowPlayingRepository : GetNowPlayingRepository,
    var getSearchedMoviesRepository : SearchedMoviesRepository
    ) : ViewModel() {

    var responseListLiveData = SingleLiveEvent<ArrayList<Result>?>()
    var responseErrorLiveData = SingleLiveEvent<Throwable?>()

    var responseSearchedListLiveData = SingleLiveEvent<ArrayList<Result>?>()
    var responseSearchedErrorLiveData = SingleLiveEvent<Throwable?>()

    fun getNowPlayingInViewModel(){
            getNowPlayingRepository.getNowPlaying({
                responseListLiveData.postValue(it?.results)
            },{
                responseErrorLiveData.postValue(it)
                println()
            })
        }

    fun getSearchingMoviesInViewModel(exampleString : String){
        getSearchedMoviesRepository.getSearchingMovies({
            responseSearchedListLiveData.postValue(it?.results)
        },{
            responseSearchedErrorLiveData.postValue(it)
            println()
        },exampleString)
    }
}