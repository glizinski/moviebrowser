package com.example.moviebrowser.api

import com.example.moviebrowser.models.DetailsOfMovieResponseItem
import com.example.moviebrowser.models.NowPlayingResponseItem
import com.example.moviebrowser.models.SearchedMoviesResponseItem
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieApi {

    @GET("/3/movie/now_playing")
    fun getNowPlaying(
        @Query("api_key") apiKey: String?,
        @Query("language") language : String?,
        @Query("region") region : String?
    ) : Observable<NowPlayingResponseItem?>

    @GET("/3/search/movie")
    fun getSearchedMovies(
        @Query("api_key") apiKey: String?,
        @Query("query") query : String?,
        @Query("language") language: String?,
        @Query("region") region : String?
    ) : Observable<SearchedMoviesResponseItem?>

    /*@GET("/3/movie/{id}")
    fun getDetailsOfMovie(
        @Path("{id}") movieId : String?,
        @Query("api_key") apiKey: String?,
        @Query("language") language: String?) : Observable<ArrayList<DetailsOfMovieResponseItem>?>*/

}