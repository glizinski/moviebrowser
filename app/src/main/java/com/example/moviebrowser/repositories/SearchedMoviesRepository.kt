package com.example.moviebrowser.repositories

import com.example.moviebrowser.BuildConfig
import com.example.moviebrowser.api.MovieApi
import com.example.moviebrowser.models.NowPlayingResponseItem
import com.example.moviebrowser.models.SearchedMoviesResponseItem
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class SearchedMoviesRepository @Inject constructor(
    private var api: MovieApi
) {
    fun getSearchingMovies(
        onDone: ((result: SearchedMoviesResponseItem?) -> Unit),
        onError: ((error: Throwable?) -> Unit),
        exampleString : String?
    ) {
        api
            .getSearchedMovies(
                BuildConfig.API_KEY,
                exampleString,
                BuildConfig.LANGUAGE,
                BuildConfig.REGION
            ) //lece do metody GET api z parametrem page
            .subscribeOn(Schedulers.newThread()) //dodanie obserwowania do nowego wątku harmonogramu
            .subscribe(                          //nasłuchiwanie czy coś się zmieniło
                {//komunikacja z serwerem przeszła pomyślnie
                    onDone.invoke(it)           //wywołanie metody z interfejsu z przypisaniem tego co zwróciło
                },

                { error -> //komunikacja z serwerem nie powiodła się
                    onDone.invoke(null)     //zwrócenie onDone z nullem co oznacza błąd
                    onError.invoke(error)          //to jest, bo tego nie mam w docelowym projekcie
                },
                {
                }
            )
    }
}