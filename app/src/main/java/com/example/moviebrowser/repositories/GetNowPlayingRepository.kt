package com.example.moviebrowser.repositories

import android.annotation.SuppressLint
import com.example.moviebrowser.BuildConfig
import com.example.moviebrowser.api.MovieApi
import com.example.moviebrowser.models.NowPlayingResponseItem
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class GetNowPlayingRepository @Inject constructor(
    private var api: MovieApi
) {
    fun getNowPlaying(
        onDone: ((result: NowPlayingResponseItem?) -> Unit),
        onError: ((error: Throwable?) -> Unit),
    ) {
        /*
        rxJava

         */
        api
            .getNowPlaying(
                BuildConfig.API_KEY,
                BuildConfig.LANGUAGE,
                BuildConfig.REGION
            ) //lece do metody GET api z parametrem page
            .subscribeOn(Schedulers.newThread()) //dodanie obserwowania do nowego wątku harmonogramu
            .subscribe(                          //nasłuchiwanie czy coś się zmieniło
                {//komunikacja z serwerem przeszła pomyślnie
                    onDone.invoke(it)           //wywołanie metody z interfejsu z przypisaniem tego co zwróciło
                },

                { error -> //komunikacja z serwerem nie powiodła się
                    onDone.invoke(null)     //zwrócenie onDone z nullem co oznacza błąd
                    onError.invoke(error)          //to jest, bo tego nie mam w docelowym projekcie
                },
                {
                }
            )
    }
}