package com.example.moviebrowser.models


import com.google.gson.annotations.SerializedName

data class SearchedMoviesResponseItem(
    @SerializedName("page")
    var page: Int?,
    @SerializedName("results")
    var results: ArrayList<Result>?,
    @SerializedName("total_pages")
    var totalPages: Int?,
    @SerializedName("total_results")
    var totalResults: Int?
)